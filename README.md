Trade Validator application can be used to validate trades (FX Spot, Forward, Options) according to defined business rules. 

It exposes 2 endpoints:

**POST /trade - to validate single trade**

example payload:
```
{
    "customer": "PLUTO1",
    "ccyPair": "EURUSD",
    "type": "Spot",
    "direction": "BUY",
    "tradeDate": "2016-08-11",
    "amount1": 1000000.00,
    "amount2": 1120000.00,
    "rate": 1.12,
    "valueDate": "2016-08-15",
    "legalEntity": "CS Zurich",
    "trader": "Johann Baumfiddler"
}
```
example response:
```
{
    "trade": {
        "type": "Spot",
        "customer": "PLUTO1",
        "ccyPair": "EURUSD",
        "direction": "BUY",
        "tradeDate": "2016-08-11",
        "amount1": 1000000,
        "amount2": 1120000,
        "rate": 1.12,
        "legalEntity": "CS Zurich",
        "trader": "Johann Baumfiddler",
        "valueDate": "2016-08-15"
    },
    "valid": true,
    "errors": []
}
```

**POST /trades** - to validate list of trades 

It accepts list of trade objects and returns list of responses 