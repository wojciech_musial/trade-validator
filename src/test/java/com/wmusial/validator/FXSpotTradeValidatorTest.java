package com.wmusial.validator;

import com.wmusial.build.FXSpotTradeBuilder;
import com.wmusial.model.FXSpotTrade;
import com.wmusial.model.Trade;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.springframework.util.Assert;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.temporal.TemporalAdjuster;
import java.time.temporal.TemporalAdjusters;

import static com.wmusial.model.Trade.TradeType.Spot;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

public class FXSpotTradeValidatorTest {

    private TradeValidator tradeValidator;
    private Trade trade;

    @Test
    public void shouldValidateValidTrade() {
        trade = new FXSpotTradeBuilder()
                .withType(Spot)
                .withCustomer("PLUTO1")
                .withLegalEntity("CS Zurich")
                .withCcyPair("EURUSD")
                .withValueDate(LocalDate.of(2018, Month.MARCH, 8))
                .withTradeDate(LocalDate.of(2018, Month.MARCH, 6))
                .build();
        tradeValidator = new FXSpotTradeValidator(trade);

        tradeValidator.validate();

        assertThat(tradeValidator.isValid(), is(true));
    }

    @Test
    public void shouldValidateNotSupportedCustomer() {
        trade = new FXSpotTradeBuilder()
                .withType(Spot)
                .withCustomer("not supported")
                .withLegalEntity("CS Zurich")
                .withCcyPair("EURUSD")
                .withValueDate(LocalDate.of(2018, Month.MARCH, 8))
                .withTradeDate(LocalDate.of(2018, Month.MARCH, 6))
                .build();
        tradeValidator = new FXSpotTradeValidator(trade);

        tradeValidator.validate();

        assertThat(tradeValidator.isValid(), is(false));
        assertThat(tradeValidator.getErrors(), hasSize(1));
    }

    @Test
    public void shouldValidateWrongValueDate() {
        trade = new FXSpotTradeBuilder()
                .withType(Spot)
                .withCustomer("PLUTO1")
                .withLegalEntity("CS Zurich")
                .withCcyPair("EURUSD")
                .withValueDate(LocalDate.of(2018, Month.MARCH, 9))
                .withTradeDate(LocalDate.of(2018, Month.MARCH, 6))
                .build();
        tradeValidator = new FXSpotTradeValidator(trade);

        tradeValidator.validate();

        assertThat(tradeValidator.isValid(), is(false));
        assertThat(tradeValidator.getErrors(), hasSize(1));
    }

    @Test
    public void shouldValidateWrongValueDateAndWeekend() {
        trade = new FXSpotTradeBuilder()
                .withType(Spot)
                .withCustomer("PLUTO1")
                .withLegalEntity("CS Zurich")
                .withCcyPair("EURUSD")
                .withValueDate(LocalDate.of(2018, Month.MARCH, 11))
                .withTradeDate(LocalDate.of(2018, Month.MARCH, 6))
                .build();
        tradeValidator = new FXSpotTradeValidator(trade);

        tradeValidator.validate();

        assertThat(tradeValidator.isValid(), is(false));
        assertThat(tradeValidator.getErrors(), hasSize(2));
    }

    @Test
    public void shouldReturnIsWeekend() {
        LocalDate saturday = LocalDate.now().with(TemporalAdjusters.next(DayOfWeek.SATURDAY));
        LocalDate sunday = LocalDate.now().with(TemporalAdjusters.next(DayOfWeek.SUNDAY));

        assertThat(TradeValidator.isWeekend(saturday), is(true));
        assertThat(TradeValidator.isWeekend(sunday), is(true));
    }

    @Test
    public void shouldReturnIsNotWeekend() {
        LocalDate monday = LocalDate.now().with(TemporalAdjusters.next(DayOfWeek.MONDAY));
        LocalDate friday = LocalDate.now().with(TemporalAdjusters.next(DayOfWeek.FRIDAY));

        assertThat(TradeValidator.isWeekend(monday), is(false));
        assertThat(TradeValidator.isWeekend(friday), is(false));
    }
}