package com.wmusial.controller;

import com.wmusial.model.Trade;
import com.wmusial.model.ValidationResponse;
import com.wmusial.service.TradeValidatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class TradeValidatorRestController {

    private final TradeValidatorService tradeValidatorService;

    @Autowired
    public TradeValidatorRestController(TradeValidatorService tradeValidatorService) {
        this.tradeValidatorService = tradeValidatorService;
    }

    @PostMapping(value = "/trade", produces = MediaType.APPLICATION_JSON_VALUE)
    public ValidationResponse validate(@RequestBody Trade trade) {
        return tradeValidatorService.validate(trade);
    }

    @PostMapping(value = "/trades", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ValidationResponse> validate(@RequestBody List<Trade> trades) {
        return trades.stream()
                .map(tradeValidatorService::validate)
                .collect(Collectors.toList());
    }
}
