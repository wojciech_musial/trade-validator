package com.wmusial.validator;

import com.wmusial.model.ForwardTrade;
import com.wmusial.model.Trade;
import com.wmusial.model.ValidationError;

import java.time.LocalDate;

public class ForwardTradeValidator extends FuturesTradeValidator {
    public ForwardTradeValidator(Trade trade) {
        super(trade);
    }

    @Override
    public void validateTradeSpecificRules() {
        validateValueDate();
    }

    private void validateValueDate() {
        LocalDate valueDate = ((ForwardTrade) trade).getValueDate();
        LocalDate tradeDate = trade.getTradeDate();

        super.validateTradeSpecificRules();
        validateValueDateValidForForward(valueDate, tradeDate);
    }

    private void validateValueDateValidForForward(LocalDate valueDate, LocalDate tradeDate) {
        // value date should be after spot date
        // http://www.londonfx.co.uk/valdates.html
        LocalDate expectedValueDate = calculateSpotDate(tradeDate);
        if (valueDate.isBefore(expectedValueDate)) {
            errors.add(new ValidationError(String.format("Value Date is incorrect for Forward trade on %s, should be: %s or later", tradeDate, expectedValueDate)));
        }
    }
}
