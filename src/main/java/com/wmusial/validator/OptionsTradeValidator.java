package com.wmusial.validator;

import com.wmusial.model.OptionsTrade;
import com.wmusial.model.Trade;
import com.wmusial.model.ValidationError;

import java.time.LocalDate;
import java.util.Objects;

public class OptionsTradeValidator extends TradeValidator {
    public OptionsTradeValidator(Trade trade) {
        super(trade);
    }

    @Override
    public void validateTradeSpecificRules() {
        OptionsTrade optionsTrade = (OptionsTrade) trade;

        LocalDate tradeDate = optionsTrade.getTradeDate();
        LocalDate expiryDate = optionsTrade.getExpiryDate();
        LocalDate excerciseStartDate = optionsTrade.getExcerciseStartDate();
        LocalDate premiumDate = optionsTrade.getPremiumDate();
        LocalDate deliveryDate = optionsTrade.getDeliveryDate();

        if (Objects.equals(optionsTrade.getStyle(), "AMERICAN")) {
            validateExerciseStartDate(tradeDate, expiryDate, excerciseStartDate);
        }

        validateExpiryDate(expiryDate, deliveryDate);
        validatePremiumDate(premiumDate, deliveryDate);
    }

    private void validateExerciseStartDate(LocalDate tradeDate, LocalDate expiryDate, LocalDate exerciseStartDate) {
        if (!exerciseStartDate.isAfter(tradeDate)) {
            errors.add(new ValidationError("Exercise Start Date should be after Trade Date for American style option trade"));
        }

        if (!exerciseStartDate.isBefore(expiryDate)) {
            errors.add(new ValidationError("Exercise Start Date should be before Expiry Date for American style option trade"));
        }
    }

    private void validatePremiumDate(LocalDate premiumDate, LocalDate deliveryDate) {
        if (!premiumDate.isBefore(deliveryDate)) {
            errors.add(new ValidationError("Premium Date should be before Delivery Date"));
        }
    }

    private void validateExpiryDate(LocalDate expiryDate, LocalDate deliveryDate) {
        if (!expiryDate.isBefore(deliveryDate)) {
            errors.add(new ValidationError("Expiry Date should be before Delivery Date"));
        }
    }
}
