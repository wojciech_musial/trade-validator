package com.wmusial.validator;

import com.wmusial.model.Trade;
import com.wmusial.model.ValidationError;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Currency;
import java.util.EnumSet;
import java.util.List;
import java.util.Objects;

import static com.wmusial.service.impl.TradeValidatorServiceImpl.SUPPORTED_CUSTOMERS;
import static com.wmusial.service.impl.TradeValidatorServiceImpl.SUPPORTED_LEGAL_ENTITIES;
import static java.time.DayOfWeek.SATURDAY;
import static java.time.DayOfWeek.SUNDAY;

public abstract class TradeValidator {

    protected final Trade trade;
    protected List<ValidationError> errors;

    public TradeValidator(Trade trade) {
        this.trade = trade;
        this.errors = new ArrayList<>();
    }

    public abstract void validateTradeSpecificRules();

    public void validate() {
        validateCustomer();
        validateLegalEntity();
        validateCurrenciesPair();
        validateTradeSpecificRules();
    }

    private void validateCustomer() {
        String customer = trade.getCustomer();

        if (!SUPPORTED_CUSTOMERS.contains(customer)) {
            errors.add(new ValidationError(customer + " customer is not supported"));
        }
    }

    private void validateLegalEntity() {
        String legalEntity = trade.getLegalEntity();

        if (!SUPPORTED_LEGAL_ENTITIES.contains(legalEntity)) {
            errors.add(new ValidationError(legalEntity + " is not supported legal entity"));
        }
    }

    private void validateCurrenciesPair() {
        String ccyPair = trade.getCcyPair();

        Objects.requireNonNull(ccyPair);

        if (ccyPair.length() != 6) {
            errors.add(new ValidationError(ccyPair + " contains invalid ISO4217 currency codes"));
            return;
        }

        String currency1 = ccyPair.substring(0, 3);
        String currency2 = ccyPair.substring(3, 6);

        try {
            Currency.getInstance(currency1);
            Currency.getInstance(currency2);
        } catch (IllegalArgumentException e) {
            errors.add(new ValidationError(ccyPair + " contains invalid ISO4217 currency codes"));
        }
    }

    protected static boolean isWeekend(LocalDate date) {
        return EnumSet.of(SATURDAY, SUNDAY)
                .contains(date.getDayOfWeek());
    }

    public boolean isValid() {
        return errors.isEmpty();
    }

    public List<ValidationError> getErrors() {
        return errors;
    }
}
