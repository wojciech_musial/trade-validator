package com.wmusial.validator;

import com.wmusial.model.FXSpotTrade;
import com.wmusial.model.Trade;
import com.wmusial.model.ValidationError;

import java.time.LocalDate;

public class FXSpotTradeValidator extends FuturesTradeValidator {

    public FXSpotTradeValidator(Trade trade) {
        super(trade);
    }

    @Override
    public void validateTradeSpecificRules() {
        LocalDate valueDate = ((FXSpotTrade) trade).getValueDate();
        LocalDate tradeDate = trade.getTradeDate();

        super.validateTradeSpecificRules();
        validateValueDateValidForSpot(valueDate, tradeDate);
    }

    private void validateValueDateValidForSpot(LocalDate valueDate, LocalDate tradeDate) {
        // value date for spot should be T+2 (or T+1 for USDCAD)
        // http://www.londonfx.co.uk/valdates.html
        LocalDate expectedValueDate = calculateSpotDate(tradeDate);
        if (!expectedValueDate.isEqual(valueDate)) {
            errors.add(new ValidationError(String.format("Value Date is incorrect for FX Spot trade on %s, should be: %s", tradeDate, expectedValueDate)));
        }
    }
}
