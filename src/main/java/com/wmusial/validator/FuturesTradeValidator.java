package com.wmusial.validator;

import com.wmusial.model.FuturesTrade;
import com.wmusial.model.Trade;
import com.wmusial.model.ValidationError;

import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.Objects;

import static java.time.DayOfWeek.MONDAY;

public abstract class FuturesTradeValidator extends TradeValidator {

    public FuturesTradeValidator(Trade trade) {
        super(trade);
    }

    @Override
    public void validateTradeSpecificRules() {
        LocalDate valueDate = ((FuturesTrade) trade).getValueDate();
        LocalDate tradeDate = trade.getTradeDate();

        validateValueDateNotBeforeTradeDate(valueDate, tradeDate);
        validateValueDateNotOnWeekend(valueDate);
    }

    private void validateValueDateNotBeforeTradeDate(LocalDate valueDate, LocalDate tradeDate) {
        if (valueDate.isBefore(tradeDate)) {
            errors.add(new ValidationError("Value Date cannot be before Trade Date"));
        }
    }

    private void validateValueDateNotOnWeekend(LocalDate valueDate) {
        if (isWeekend(valueDate)) {
            errors.add(new ValidationError("Value Date cannot be on weekend"));
        }
    }

    protected LocalDate calculateSpotDate(LocalDate tradeDate) {
        // spot date calculation
        // http://www.londonfx.co.uk/valdates.html
        long tradeValueDateDifference = Objects.equals(trade.getCcyPair(), "USDCAD") ? 1L : 2L;
        LocalDate expectedValueDate = tradeDate.plusDays(tradeValueDateDifference);
        return isWeekend(expectedValueDate) ? expectedValueDate.with(TemporalAdjusters.next(MONDAY)) : expectedValueDate;
    }
}
