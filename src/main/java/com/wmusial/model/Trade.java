package com.wmusial.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.math.BigDecimal;
import java.time.LocalDate;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type", visible = true)
@JsonSubTypes({
        @JsonSubTypes.Type(value = FXSpotTrade.class, name = "Spot"),
        @JsonSubTypes.Type(value = ForwardTrade.class, name = "Forward"),
        @JsonSubTypes.Type(value = OptionsTrade.class, name = "VanillaOption")
})
public abstract class Trade {

    public enum TradeType {
        Spot, Forward, VanillaOption
    }

    private String customer;
    private String ccyPair;
    private TradeType type;
    private String direction;
    private LocalDate tradeDate;
    private BigDecimal amount1;
    private BigDecimal amount2;
    private BigDecimal rate;

    private String legalEntity;
    private String trader;

    protected Trade() {
    }

    public Trade(String customer, String ccyPair, TradeType type, String direction, LocalDate tradeDate,
                 BigDecimal amount1, BigDecimal amount2, BigDecimal rate, String legalEntity, String trader) {
        this.customer = customer;
        this.ccyPair = ccyPair;
        this.type = type;
        this.direction = direction;
        this.tradeDate = tradeDate;
        this.amount1 = amount1;
        this.amount2 = amount2;
        this.rate = rate;
        this.legalEntity = legalEntity;
        this.trader = trader;
    }

    public String getCustomer() {
        return customer;
    }

    public String getCcyPair() {
        return ccyPair;
    }

    public TradeType getType() {
        return type;
    }

    public String getDirection() {
        return direction;
    }

    public LocalDate getTradeDate() {
        return tradeDate;
    }

    public BigDecimal getAmount1() {
        return amount1;
    }

    public BigDecimal getAmount2() {
        return amount2;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public String getLegalEntity() {
        return legalEntity;
    }

    public String getTrader() {
        return trader;
    }
}
