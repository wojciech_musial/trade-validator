package com.wmusial.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Currency;

public class OptionsTrade extends Trade {

    private String style;
    private String strategy;
    private LocalDate deliveryDate;
    private LocalDate expiryDate;
    private String payCcy;
    private BigDecimal premium;
    private Currency premiumCcy;
    private String premiumType;
    private LocalDate premiumDate;
    private LocalDate excerciseStartDate;

    public String getStyle() {
        return style;
    }

    public String getStrategy() {
        return strategy;
    }

    public LocalDate getDeliveryDate() {
        return deliveryDate;
    }

    public LocalDate getExpiryDate() {
        return expiryDate;
    }

    public String getPayCcy() {
        return payCcy;
    }

    public BigDecimal getPremium() {
        return premium;
    }

    public Currency getPremiumCcy() {
        return premiumCcy;
    }

    public String getPremiumType() {
        return premiumType;
    }

    public LocalDate getPremiumDate() {
        return premiumDate;
    }

    public LocalDate getExcerciseStartDate() {
        return excerciseStartDate;
    }
}
