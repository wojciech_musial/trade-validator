package com.wmusial.model;

import java.math.BigDecimal;
import java.time.LocalDate;

public class FXSpotTrade extends FuturesTrade {

    protected FXSpotTrade() {
    }

    public FXSpotTrade(String customer, String ccyPair, TradeType type, String direction, LocalDate tradeDate,
                       BigDecimal amount1, BigDecimal amount2, BigDecimal rate, String legalEntity, String trader, LocalDate valueDate) {
        super(customer, ccyPair, type, direction, tradeDate, amount1, amount2, rate, legalEntity, trader, valueDate);
    }
}
