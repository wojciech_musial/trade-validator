package com.wmusial.model;

import com.wmusial.validator.FXSpotTradeValidator;
import com.wmusial.validator.ForwardTradeValidator;
import com.wmusial.validator.OptionsTradeValidator;
import com.wmusial.validator.TradeValidator;

public class TradeValidatorFactory {

    public static TradeValidator getInstance(Trade trade) {
        switch (trade.getType()) {
            case Spot:
                return new FXSpotTradeValidator(trade);
            case Forward:
                return new ForwardTradeValidator(trade);
            case VanillaOption:
                return new OptionsTradeValidator(trade);
            default:
                throw new UnsupportedOperationException(String.format("There is no validator for %s trade type", trade.getType()));
        }
    }
}
