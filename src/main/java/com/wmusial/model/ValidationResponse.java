package com.wmusial.model;

import java.util.List;

public class ValidationResponse {
    private final Trade trade;
    private final boolean valid;
    private final List<ValidationError> errors;

    public ValidationResponse(Trade trade, List<ValidationError> errors) {
        this.trade = trade;
        this.errors = errors;
        this.valid = errors.isEmpty();
    }

    public Trade getTrade() {
        return trade;
    }

    public boolean isValid() {
        return valid;
    }

    public List<ValidationError> getErrors() {
        return errors;
    }
}
