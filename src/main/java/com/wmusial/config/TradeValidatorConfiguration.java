package com.wmusial.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.wmusial")
public class TradeValidatorConfiguration {
}
