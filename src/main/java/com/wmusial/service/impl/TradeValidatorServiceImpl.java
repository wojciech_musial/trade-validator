package com.wmusial.service.impl;

import com.wmusial.model.Trade;
import com.wmusial.model.TradeValidatorFactory;
import com.wmusial.model.ValidationResponse;
import com.wmusial.service.TradeValidatorService;
import com.wmusial.validator.TradeValidator;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@Service
public class TradeValidatorServiceImpl implements TradeValidatorService {

    public static final Set<String> SUPPORTED_CUSTOMERS = new HashSet<>(Arrays.asList("PLUTO1", "PLUTO2"));
    public static final Set<String> SUPPORTED_LEGAL_ENTITIES = new HashSet<>(Collections.singletonList("CS Zurich"));

    @Override
    public ValidationResponse validate(Trade trade) {
        TradeValidator validator = TradeValidatorFactory.getInstance(trade);

        validator.validate();

        return new ValidationResponse(trade, validator.getErrors());
    }
}
