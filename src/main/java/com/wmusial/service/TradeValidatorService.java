package com.wmusial.service;

import com.wmusial.model.Trade;
import com.wmusial.model.ValidationResponse;

public interface TradeValidatorService {

    ValidationResponse validate(Trade trade);
}
